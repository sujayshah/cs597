#include "perm-lexicographical.h"
#include <algorithm>
#include <iostream>


bool nextPermLexicographical(std::vector<int> &p)
{
	int i = p.size() - 1;
	while (i > 0 && p[i - 1] >= p[i])
		i--;
	
	if (i <= 0)
		return false;

	int leftIndex = i - 1;
	// rightIndex
	int j = p.size() - 1;
	while (p[j] <= p[leftIndex])
		j--;

	std::swap(p[leftIndex],p[j]);
	//reverse tail
	std::reverse(p.begin() + i,p.end());
	return true;
}
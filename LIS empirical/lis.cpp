#include "lis.h"
#include <iostream>

int longest_increasing_subsequence( std::vector<int> const& arr ) {
    int n,i, j, max = 0;
    n=arr.size();
    std::vector<int> lis;
	lis.reserve(n);
    /* Initialize LIS values for all indexes */
    for (i = 0; i < n; i++ )
        lis[i] = 1;
 
    /* Compute optimized LIS values in bottom up manner */
    for (i = 1; i < n; i++ )
        for (j = 0; j < i; j++ ) 
            if ( arr[i] > arr[j] && lis[i] < lis[j] + 1)
                lis[i] = lis[j] + 1;
 
    /* Pick maximum of all LIS values */
    for (i = 0; i < n; i++ )
        if (max < lis[i])
            max = lis[i];
 
 
    return max;
}

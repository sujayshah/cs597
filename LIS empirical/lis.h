#ifndef LIS_H
#define LIS_H
#include <vector>
int longest_increasing_subsequence( std::vector<int> const& sequence );
#endif

#include "lis.h"
#include <iterator>
#include <algorithm>
#include <iostream>
#include <cstdio> //sscanf
#include <ctime>   //std::time
#include <cstdlib> //std::srand, std::rand

void create_sequence_of_size( int size, std::vector<int>& sequence ) {
    sequence.reserve( size );
    for ( int i=0; i<size; ++i ) {
        sequence.push_back( std::rand()%(10*size) );
    }
}

int main( int argc, char ** argv) {
    std::srand ( std::time( NULL ) );
    if (argc!=2) return 1;
    else {
		int len = 0;
		std::sscanf(argv[1],"%i",&len);
        int num_exp = 200;
        std::vector<int> data( num_exp );
        for ( int i=0; i<num_exp; ++i ) {
            std::vector<int> sequence;
            create_sequence_of_size( len, sequence );
            data.push_back( longest_increasing_subsequence( sequence ) );
            std::cout << data.back() << " ";
        }
    }
    return 0;
}

#include "perm-jt.h"
#include <iostream>
using namespace std;

bool PermJohnsonTrotter::isMobile(std::vector<int> const& e , int currentIndex)
{
	int i=currentIndex;

	if(i + dir[i]< e.size() && e[i] > e[i+dir[i]] )
		return true;
	else 
		return false;
}
PermJohnsonTrotter::PermJohnsonTrotter(int size)
{
	for(int i=0; i<size; ++i)
	{
		element.push_back(i+1);
		dir.push_back(-1);
	}
}

std::vector<int> const& PermJohnsonTrotter::Get() const
{
	return element;
}

bool PermJohnsonTrotter::Next()
{
	int largestMobile=-1;
	int largestMobileIndex=-1;
	int temp,tempDir,index2Swap;
	for(int i=0;i<element.size();++i)
	{
		if(element[i]>largestMobile && isMobile(element,i))
		{
			largestMobile=element[i];
			largestMobileIndex=i;
		}
	}
	if(largestMobileIndex==-1)
		return false;
	else
	{
		
		temp = element[largestMobileIndex];
		index2Swap = largestMobileIndex + dir[largestMobileIndex];
		element[largestMobileIndex]=element[index2Swap];
		element[index2Swap] = temp;
		
		//cout << "swapped elements at " << largestMobileIndex << "  and " << index2Swap << endl;

		tempDir = dir[largestMobileIndex];
		dir[largestMobileIndex] = dir[index2Swap];
		dir[index2Swap] = tempDir;

		//step 3
		for (int i = 0; i < element.size(); ++i)
		{
			if (element[i] > temp)
				dir[i] *= -1;
		}
		return true;
	}
}	


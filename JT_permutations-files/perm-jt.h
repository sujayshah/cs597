#ifndef PERM_JOHNSON_TROTTER_H
#define PERM_JOHNSON_TROTTER_H
#include <vector>

class PermJohnsonTrotter {
	
	std::vector<int> element;
	std::vector<int> dir;
	
	public:
		PermJohnsonTrotter(int size);
		bool Next();
		std::vector<int> const& Get() const;
		bool isMobile(std::vector<int> const& e , int currentIndex);
};
#endif

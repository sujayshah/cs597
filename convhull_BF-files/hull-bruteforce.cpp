#include "hull-bruteforce.h"
#include <algorithm>
#include <iostream>

bool Point::operator==( Point const& arg2 ) const {
    return ( (x==arg2.x) && (y==arg2.y) );
}

std::ostream& operator<< (std::ostream& os, Point const& p) {
	os << "(" << p.x << " , " << p.y << ") ";
	return os;
}

std::istream& operator>> (std::istream& os, Point & p) {
	os >> p.x >> p.y;
	return os;
}

int orient(Point p1,Point p2, Point p3)
{
	int val = (p2.y - p1.y)*(p3.x - p2.x) - (p2.x - p1.x)*(p3.y-p2.y);

	if (val == 0)
		return 0;

	return (val > 0) ? 1 : 2;
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//return value is (on left, on right)
std::pair<bool,bool> get_location(
		float const& p1x, //check which side of the line (p1x,p1y)-->(p2x,p2y) 
		float const& p1y, //point (qx,qy) is on
		float const& p2x,
		float const& p2y,
		float const& qx,
		float const& qy
		) 
{
	Point dir   = {p2x-p1x,p2y-p1y};
	Point norm  = {dir.y, -dir.x};
	Point point = {qx-p1x,qy-p1y};
	//scalar product is positive if on right side
	float scal_prod = norm.x*point.x + norm.y*point.y;
	return std::make_pair( (scal_prod<0), (scal_prod>0));
}

//returns a set of indices of points that form convex hull
std::set<int> hullBruteForce ( std::vector< Point > const& points ) {
	int num_points = points.size();
	//std::cout << "number of points " << num_points << std::endl;
	if ( num_points < 3 ) throw "bad number of points";

	std::set<int> hull_indices;	
	std::pair<bool, bool> location;
	bool same_side;
	for ( int i=0;i<num_points-1;++i)
	{
		for ( int j = i + 1; j < num_points; ++j)
		{
			same_side = true;
			int iter = j + 1;

			//determine side
			determine:
			location = get_location(points[i].x, points[i].y, points[j].x, points[j].y, points[iter%num_points].x, points[iter%num_points].y);
			if (location.first == false && location.second == false) //pt on line
			{
				++iter;
				//location = get_location(points[i].x, points[i].y, points[j].x, points[j].y, points[(iter)%num_points].x, points[(iter)%num_points].y);
				goto determine;
			}

			if (location.first)
			{
				for ( int k = j + 1; k<(num_points + iter) && same_side; ++k)
				{
					location = get_location(points[i].x, points[i].y, points[j].x, points[j].y, points[k%num_points].x, points[k%num_points].y);

					if (location.first == false && location.second == false) //pt on line
						continue;

					same_side = same_side && location.first;
					//same_side = same_side && location.second;

					if (!same_side)
						break;
				}
			}
			else if(location.second)
			{
				for ( int k = j + 1; k<(num_points + iter) && same_side; ++k)
				{
					location = get_location(points[i].x, points[i].y, points[j].x, points[j].y, points[k%num_points].x, points[k%num_points].y);

					if (location.first == false && location.second == false) //pt on line
						continue;

					same_side = same_side && location.second;
					
					//same_side = same_side && location.second;

					if (!same_side)
						break;
				}
			}
			if (same_side)
			{
				hull_indices.insert(i);
				hull_indices.insert(j);
			}
		}
	}
	return hull_indices;
}



std::vector<int> hullBruteForce2 ( std::vector< Point > const& points ) {
	int num_points = points.size();
	if ( num_points < 3 ) throw "bad number of points";

	std::vector<int> hull_indices;

	float minX = 99999.0f;
	int smallestIndexOfX=-1;

	for (int i = 0; i < num_points; ++i)
	{
		if (points[i].x < minX)
		{
			minX = points[i].x;
			smallestIndexOfX = i;
		}
	}

	int p = smallestIndexOfX,q;
	do
	{
		hull_indices.push_back(p);
		q = (p + 1) % num_points;

		for (int i = 0; i < num_points; ++i)
		{
			if (orient(points[p], points[i], points[q]) == 2)
				q = i;
		}
		p = q;
	} while (p!= smallestIndexOfX);

	return hull_indices;
}



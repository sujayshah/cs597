//to compile g++ -std=c++11 -O3 
//to run my.exe 200 1000 
//first argument us the number of tests, second array size
#include <limits>
#include <iostream>
#include <algorithm>
#include <cstdio>
#include <random>
#include <ctime>

int main( int argc, char** argv ) 
{
	//if (argc!=2) return 1;
    int start_s=clock();
    int size = 0;
    std::sscanf(argv[1],"%i",&size);
    
    int num_tests = 200;
    //std::sscanf(argv[1],"%i",&num_tests);
   
    // create array size n with values [-n/2,n/2] !!!!!!!!!!!!!!!!!!!!!!!!!
    int *a = new int[ size ];
	std::cout<<"Array before shuffling : \n"<<std::endl;
    for (int i = 0; i < size; ++i) { 
        a[i] = i - size/2;
		std::cout<<a[i]<<"\t";
    }
	std::cout<<std::endl<<"\n";
    // C++11 RNG
    std::random_device rd;
    std::mt19937 gen( rd() ); // random engine
    
    for ( int experiment =0; experiment < num_tests; ++experiment ) {
        std::shuffle( a, a+size, gen ); // C++11

		std::cout<<"Experiment # : "<<experiment<<std::endl;
		std::cout<<"Array after shuffle : "<<std::endl;
		for(int i=0;i<size;++i)
			std::cout<<a[i]<<"\t";
		
		std::cout<<std::endl;
		
        int max_so_far = a[0], max_ending_here = a[0];

        for (int i = 0; i < size; ++i)
        {
            max_ending_here = max_ending_here + a[i];
            if (max_so_far < max_ending_here)
                max_so_far = max_ending_here;

            if (max_ending_here < 0)
                max_ending_here = 0;
        }

        std::cout <<"Max so far :"<< max_so_far << "\n\n\n";
    }
    delete [] a;
    int stop_s=clock();
    std::cout << "Execution Time(in ms): " << (stop_s-start_s)/double(CLOCKS_PER_SEC)*1000 << std::endl;
}